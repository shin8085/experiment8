package com.example.experiment8;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

@Route(path = "/experiment8/MainActivity")
public class MainActivity extends AppCompatActivity {


    AlertDialog.Builder builder;
    List<String> filespath;
    @BindView(R.id.videoList)
    ListView videoList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ARouter.openLog();
        ARouter.openDebug();
        ARouter.init(getApplication());
        //获取权限
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){

            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
        }
        showVideoFiles();
    }
    private void showVideoFiles(){
        Cursor c  = this.getContentResolver()
                .query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                        new String[]{MediaStore.Video.Media.TITLE,
                                MediaStore.Video.Media.DURATION,
                                MediaStore.Video.Media._ID,
                                MediaStore.Video.Media.DISPLAY_NAME ,
                                MediaStore.Video.Media.DATA},
                        null, null, null);
        if (c==null || c.getCount()==0)     //如果没有搜索到视频，显示存储列表为空...
        {
            builder = new AlertDialog.Builder(this);
            builder.setMessage("存储列表为空...").setPositiveButton("确定", null);
            AlertDialog ad = builder.create();
            ad.show();
        }
        else{
            String listItems[]=new String[c.getCount()];
            filespath=new ArrayList<>();
            int index=0;
            while (c.moveToNext()) {
                listItems[index++]=c.getString(0);
                filespath.add(c.getString(4));
            }
            ArrayAdapter<String> adapter=new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,listItems);
            videoList.setAdapter(adapter);
            c.close();
        }
    }

    @OnItemClick(R.id.videoList)
    public void openVideo(int pos){
        ARouter.getInstance().build("/experiment8/PlayVideo").withString("path",filespath.get(pos)).navigation();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission Granted} else { // Permission Denied}
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences=getSharedPreferences("videoProgress",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putInt("index",0);
        editor.commit();
    }
}
